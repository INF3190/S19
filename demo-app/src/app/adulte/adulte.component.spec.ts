import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdulteComponent } from './adulte.component';

describe('AdulteComponent', () => {
  let component: AdulteComponent;
  let fixture: ComponentFixture<AdulteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdulteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdulteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
