import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnaineComponent } from './enaine.component';

describe('EnaineComponent', () => {
  let component: EnaineComponent;
  let fixture: ComponentFixture<EnaineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnaineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
