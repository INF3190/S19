import { Component } from '@angular/core';
import * as liste from '../assets/data/liste.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Site de la séance 19';
  personnes:any = (liste as any).default;// default est une clé qui s'ajoute par defaut pour un tableau sans clé
  min:any={};
  max:any={};
  titreage:string="Benjamin";
  titrebouton: string = "Doyen"
  leplus:string = "";
  affichage:any={};
  ngOnInit() {

     this.benjamin();
     this.affichage = this.min;

    console.log(this.personnes);
  }

 benjamin(){
   let ageMinimum=this.personnes[0];
   for(let i of this.personnes){
     if(i.age<ageMinimum.age){
       ageMinimum=i;
     }
   }
   this.min=ageMinimum;
   this.titreage="Benjamin";
   this.titrebouton = "Doyen";
   this.leplus = "jeune";
   //this.agemoyen();
 }

 doyen(){
  let ageMinimum=this.personnes[0];
  for(let i of this.personnes){
    if(i.age>ageMinimum.age){
      ageMinimum=i;
    }
  }
  this.max=ageMinimum;
  this.titreage="Doyen";
   this.titrebouton = "Benjamin";
   this.leplus = "vieux";
   //this.agemoyen();
}

toggleAge(){
  if(this.affichage.age==this.min.age){
    this.doyen();
    this.affichage=this.max;
  }else{
    this.benjamin();
    this.affichage=this.min;
  }
  this.agemoyen();
}

  agemoyen() {
    let som = 0;

    for (let person of this.personnes) {
      som += person.age;
    }
    this.affichage.amoyen = som / this.personnes.length;
}

}
