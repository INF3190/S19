import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdulteComponent } from './adulte/adulte.component';
import { AineComponent } from './aine/aine.component';
import { EnaineComponent } from './enaine/enaine.component';

@NgModule({
  declarations: [
    AppComponent,
    AdulteComponent,
    AineComponent,
    EnaineComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
