import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdulteComponent } from './adulte/adulte.component';
import { AineComponent } from './aine/aine.component';
import { AppComponent } from './app.component';
import { EnaineComponent } from './enaine/enaine.component';

const routes: Routes = [
  { path: 'adultes', component: AdulteComponent },
  { path: 'aines', component: AineComponent },
  { path: 'retraites', redirectTo: '/aines' },
  { path: 'en/aines', component: EnaineComponent },
  { path: '',redirectTo:'/en/aines', pathMatch:'full'},
  { path: 'en/:name',redirectTo:'/adultes', pathMatch:'full'},
  { path: '**', component: AineComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
